#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
 * Illustrate a simple send and receive
 */
void demo_sendrecv1()
{
    int rank, nproc;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    /* Demo simple send and receive */
    if (rank == 0) {
        int data;
        for (int p = 1; p < nproc; ++p) {
            MPI_Recv(&data, 1, MPI_INT, p, 0, 
                     MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("1. P0: Received %d from %d\n", data, p);
        }
    } else {
        int data = rank*10;
        MPI_Send(&data, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
}


/*
 * Illustrate cyclic data passing with combined sendrecv
 */
void demo_sendrecv2()
{
    int rank, nproc;
    int sendbuf;
    int recvbuf;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    
    /* 
     * Processors form a logical ring; each processor sends to right and
     * receives from left.
     */
    sendbuf = 10*rank;
    MPI_Sendrecv(&sendbuf, 1, MPI_INT, (rank+1) % nproc, 1,
                 &recvbuf, 1, MPI_INT, (rank+nproc-1) % nproc, 1,
                 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    printf("2. P%d: Sent %d, received %d\n", rank, sendbuf, recvbuf);
}


/*
 * Demonstrate a broadcast and gather
 */
void demo_bcast_gather()
{
    int rank, nproc;
    int buffer;
    int* results;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    
    /* 
     * Processor 0 broadcasts the value in buffer to everyone.
     * Note that MPI_Bcast is called on *all* processors.
     */
    if (rank == 0) {
        buffer = 10;
        printf("3. Broadcasting %d\n", buffer);
    }
    MPI_Bcast(&buffer, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
    /*
     * Everyone now computes 10*rank and sends the result to
     * be gathered at rank 0.  Note that the results buffer is
     * not used except at the root (rank 0).
     */
    buffer *= rank;
    if (rank == 0) {
        printf("3. Gathering at rank 0\n");
        results = (int*) malloc(nproc * sizeof(int));
    }
    MPI_Gather(&buffer, 1, MPI_INT, 
               results, 1, MPI_INT,
               0, MPI_COMM_WORLD);
    if (rank == 0) {
        for (int p = 0; p < nproc; ++p)
            printf("3. P%d sent %d\n", p, results[p]);
        free(results);
    }
}


/*
 * Demonstrate a reduce operation
 */
void demo_reduce()
{
    int rank, nproc, my_data, sum;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    
    my_data = rank+1;
    MPI_Reduce(&my_data, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    if (rank == 0) 
        printf("4. Sum was %d (should be %d)\n", sum, nproc*(nproc+1)/2);
}


int main(int argc, char** argv)
{
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    
    if (rank == 0)
        printf("Running MPI demos\n");
    demo_sendrecv1();

    /* Barriers are here in a (possibly vain) attempt to synchronize I/O */
    MPI_Barrier(MPI_COMM_WORLD);
    demo_sendrecv2();
    
    MPI_Barrier(MPI_COMM_WORLD);
    demo_bcast_gather();
    
    MPI_Barrier(MPI_COMM_WORLD);
    demo_reduce();
    
    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Finalize();
    return 0;
}

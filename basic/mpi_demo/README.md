# MPI demos

The MPI demo file illustrates several MPI communication functions:
sends, receives, gathers, reductions, and broadcast.

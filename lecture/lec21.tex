\documentclass{beamer}

\mode<presentation>
{
%  \usetheme[hideothersubsections]{PaloAlto}
  \usetheme{default}
  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

\usepackage{amsmath,amssymb}
%\usepackage{times} 
\usepackage[T1]{fontenc}

%\usetheme{Berlin}

\newcommand{\bfx}{\mathbf{x}}
\newcommand{\bfr}{\mathbf{r}}
\newcommand{\bfn}{\mathbf{n}}
\newcommand{\bfI}{\mathbf{I}}
\newcommand{\bbR}{\mathbb{R}}


\title[CS 5220, Spring 2014]{Lecture 21: \\
  Graph Partitioning}

\author[]{David Bindel} \date[]{15 Apr 2014}


\begin{document}

% Graph partitioning task + terminology
% Applications
% Coordinate-based:
%   Coordinate bisection
%   Inertial partitioning
%   Circle partitioning (beyond hyperplanes) a la Gilbert
%     = centerpoint + random great circle
% Coordinate-free:
%   BFS / recursive graph bisection
%   Kernighan-Lin
%   Spectral partitioning
% Multilevel ideas
% Software

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Logistics}

  \begin{itemize}
  \item Have until May 2 for topic mining
    \begin{itemize}
    \item You {\em should not} need all that time
    \item You {\em should} start early anyhow
    \item This should {\em not} block work on your final project
    \end{itemize}
  \item Guest lectures: two of 4/22, 4/24, 4/29
    \begin{itemize}
    \item One on CUDA programming
    \item One on FFT
    \item One for you to work on your final project!
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Project logistics}
  
  \begin{itemize}
  \item Project proposal feedback posted
    \begin{itemize}
    \item Common theme: Don't be too ambitious!
    \item Focusing on a key kernel is fine
    \end{itemize}
  \item Project presentations outside class (May 1--May 7)
    \begin{itemize}
    \item Think about 5 minutes of you talking
    \item Give me some background and preliminary results
    \item I will set up a schedule
    \end{itemize}
  \item Project write-up: by morning of May 15 (one month)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Topic mining logistics}
  
  \begin{itemize}
  \item Easiest way to run on $N$ processors (all one node): \\
        \begin{center}
          {\tt ompsub -n N julia -p N driver.jl}
        \end{center}
  \item Run twice and take the second timing
    \begin{itemize}
    \item Julia uses a JIT compiler -- adds to initial run time
    \end{itemize}
  \item You may want to try a bigger data set
    \begin{itemize}
    \item I recommend the NIPS data
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Topic mining algorithmics}

  \begin{itemize}
  \item Choosing a good $\eta$ is nontrivial
    \begin{itemize}
    \item I chose $\eta_k = 2000 \tau^k$ for $\tau = 0.99$
    \item This is really large!
    \item You may have a better strategy
    \end{itemize}
  \item Warm start may help
    \begin{itemize}
    \item Warning: EG gets stuck when start vector has zeros!
    \end{itemize}
  \item Feel free to use a mixed strategy (EG + active set fallback)
    \begin{itemize}
    \item Idea: use EG + rounding to get AS start point
    \item Main cost in active set is finding which entries are zero
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Topic mining tuning}

  \begin{itemize}
  \item I've further tuned the active set code (and pushed to
    Bitbucket).  Commentary
    \begin{itemize}
    \item Be aware of temporaries
    \item Prefer destructive operations
    \item Don't fear loops as appropriate (it's not MATLAB)
    \item Algorithmic improvements still trump tuning
    \end{itemize}
  \item Tuning resources:
    \begin{itemize}
    \item \url{http://julialang.org/blog/2013/09/fast-numeric/}
    \item \url{http://docs.julialang.org/en/latest/manual/performance-tips/}
    \item \url{http://julia.readthedocs.org/en/latest/stdlib/profile/}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Reminders}
  
  \begin{itemize}
  \item Read the code and the prompt
  \item Apply what you've learned in class
  \item Start early to ask questions
  \item {\em Leave time for your project}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    And now for something completely different.
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Graph partitioning}

  Given:
  \begin{itemize}
  \item
    Graph $G = (V,E)$ 
  \item
    Possibly weights $(W_V, W_E)$.
  \item
    Possibly coordinates for vertices (e.g. for meshes).
  \end{itemize}

  We want to partition $G$ into $k$ pieces such that
  \begin{itemize}
  \item
    Node weights are balanced across partitions.
  \item
    Weight of cut edges is minimized.
  \end{itemize}
  Important special case: $k = 2$.

\end{frame}


\begin{frame}
  \frametitle{Types of separators}

  \begin{itemize}
  \item {\em Edge} separators: remove edges to partition
  \item {\em Node} separators: remove nodes (and adjacent edges)
  \end{itemize}
  Can go from one to the other (easiest if graph is degree-bounded).

\end{frame}


\begin{frame}
  \frametitle{Why partitioning?}

  \begin{itemize}
  \item Physical network design (telephone layout, VLSI layout)
  \item Sparse matvec
  \item Preconditioners for PDE solvers
  \item Sparse Gaussian elimination
  \item Data clustering
  \item Image segmentation
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Cost}

  How many partitionings are there?  If $n$ is even,
  \[
    \begin{pmatrix} n \\ n/2 \end{pmatrix} =
    \frac{n!}{( (n/2)! )^2} \approx 
    2^n \sqrt{2/(\pi n)}.
  \]
  Finding the optimal one is NP-complete.

  \vspace{1cm}
  We need heuristics!
\end{frame}


\begin{frame}
  \frametitle{Partitioning with coordinates}

  \begin{itemize}
  \item Lots of partitioning problems from ``nice'' meshes
    \begin{itemize}
    \item Planar meshes (maybe with regularity condition)
    \item $k$-ply meshes (works for $d > 2$)
    \item Nice enough $\implies$ partition with $O(n^{1-1/d})$ edge cuts \\
      (Tarjan, Lipton; Miller, Teng, Thurston, Vavasis)
    \item Edges link nearby vertices
    \end{itemize}
  \item Get useful information from vertex density
  \item Ignore edges (but can use them in later refinement)
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Recursive coordinate bisection}

  Idea: Choose a cutting hyperplane parallel to a coordinate axis.
  \begin{itemize}
  \item Pro: Fast and simple
  \item Con: Not always great quality
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Inertial bisection}

  Idea: Optimize cutting hyperplane based on vertex density
  \begin{align*}
    \bar{\bfx} &= \frac{1}{n} \sum_{i=1}^n \bfx_i \\
    \bar{\bfr_i} &= \bfx_i-\bar{\bfx} \\
    \bfI &= \sum_{i=1}^n\left[ \|\bfr_i\|^2 I - \bfr_i \bfr_i^T \right]
  \end{align*}
  Let $(\lambda_n, \bfn)$ be the minimal eigenpair for the inertia
  tensor $\bfI$, and choose the hyperplane through $\bar{\bfx}$ 
  with normal $\bfn$.  
  
  \vspace{5mm}
  \begin{itemize}
  \item Pro: Still simple, more flexible than coordinate planes
  \item Con: Still restricted to hyperplanes
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Random circles (Gilbert, Miller, Teng)}
  
  \begin{itemize}
  \item Stereographic projection
  \item Find centerpoint (any plane is an even partition) \\
    In practice, use an approximation.
  \item Conformally map sphere, moving centerpoint to origin
  \item Choose great circle (at random)
  \item Undo stereographic projection
  \item Convert circle to separator
  \end{itemize}
  May choose best of several random great circles.
\end{frame}


\begin{frame}
  \frametitle{Coordinate-free methods}
  
  \begin{itemize}
  \item Don't always have natural coordinates
    \begin{itemize}
    \item Example: the web graph
    \item Can sometimes add coordinates (metric embedding)
    \end{itemize}
  \item So use edge information for geometry!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Breadth-first search}

  \begin{itemize}
  \item Pick a start vertex $v_0$
    \begin{itemize}
    \item Might start from several different vertices
    \end{itemize}
  \item Use BFS to label nodes by distance from $v_0$
    \begin{itemize}
    \item We've seen this before -- remember RCM?
    \item Could use a different order -- minimize edge cuts locally \\
      (Karypis, Kumar)
    \end{itemize}
  \item Partition by distance from $v_0$
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Greedy refinement}

  Start with a partition $V = A \cup B$ and refine.
  \begin{itemize}
  \item Gain from swapping $(a,b)$ is $D(a) + D(b)$, where
    \begin{align*}
      D(a) &= \sum_{b' \in B} w(a,b') - \sum_{a' \in A, a' \neq a} w(a,a') \\
      D(b) &= \sum_{a' \in A} w(b,a') - \sum_{b' \in B, b' \neq b} w(b,b') 
    \end{align*}
  \item Purely greedy strategy: 
    \begin{itemize}
    \item Choose swap with most gain
    \item Repeat until no positive gain
    \end{itemize}
  \item Local minima are a problem.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Kernighan-Lin}

  In one sweep:
  \begin{tabbing}
    \qquad \= \kill
    While no vertices marked \\
    \> Choose $(a,b)$ with greatest gain \\
    \> Update $D(v)$ for all unmarked $v$ as if $(a,b)$ were swapped \\
    \> Mark $a$ and $b$ (but don't swap) \\
    Find $j$ such that swaps $1, \ldots, j$ yield maximal gain \\
    Apply swaps $1, \ldots, j$
  \end{tabbing}
  Usually converges in a few (2-6) sweeps.  Each sweep is $O(N^3)$.
  Can be improved to $O(|E|)$ (Fiduccia, Mattheyses).
  
  \vspace{5mm}
  Further improvements (Karypis, Kumar): only consider vertices on boundary,
  don't complete full sweep.
\end{frame}


\begin{frame}
  \frametitle{Spectral partitioning}

  Label vertex $i$ with $x_i = \pm 1$.  We want to minimize
  \[
    \mbox{edges cut} = \frac{1}{4} \sum_{(i,j) \in E} (x_i-x_j)^2
  \]
  subject to the even partition requirement
  \[
    \sum_i x_i = 0.
  \]
  But this is NP hard, so we need a trick.

\end{frame}


\begin{frame}
  \frametitle{Spectral partitioning}

  Write
  \[
    \mbox{edges cut} 
    = \frac{1}{4} \sum_{(i,j) \in E} (x_i-x_j)^2 
    = \frac{1}{4} \|Cx\|^2 = \frac{1}{4} x^T L x
  \]
  where $C$ is the incidence matrix and $L = C^T C$ is the graph Laplacian:
  \begin{align*}
    C_{ij} &= 
      \begin{cases}
         1, & e_j = (i,k) \\
        -1, & e_j = (k,i) \\
         0, & \mbox{otherwise},
      \end{cases} &
    L_{ij} &= 
    \begin{cases} 
      d(i), & i = j \\
      -1, & i \neq j, (i,j) \in E, \\ 
      0, & \mbox{otherwise}.
    \end{cases}
  \end{align*}
  Note that $C e = 0$ (so $L e = 0$), $e = (1, 1, 1, \ldots, 1)^T$.

\end{frame}


\begin{frame}
  \frametitle{Spectral partitioning}

  Now consider the {\em relaxed} problem with $x \in \bbR^n$:
  \[
    \mbox{minimize } x^T L x \mbox{ s.t. } x^T e = 0 \mbox{ and } x^T x = 1.
  \]
  Equivalent to finding the second-smallest eigenvalue $\lambda_2$
  and corresponding eigenvector $x$, also called the {\em Fiedler vector}.
  Partition according to sign of $x_i$.

  \vspace{5mm}
  How to approximate $x$?  Use a Krylov subspace method (Lanczos)!
  Expensive, but gives high-quality partitions.
\end{frame}


\begin{frame}
  \frametitle{Multilevel ideas}

  Basic idea (same will work in other contexts):
  \begin{itemize}
  \item Coarsen
  \item Solve coarse problem
  \item Interpolate (and possibly refine)
  \end{itemize}
  May apply recursively.

\end{frame}


\begin{frame}
  \frametitle{Maximal matching}

  One idea for coarsening: maximal matchings
  \begin{itemize}
  \item
    {\em Matching} of $G = (V,E)$ is $E_m \subset E$ with no common vertices.
  \item
    {\em Maximal} if no more edges can be added and remain matching.
  \item
    Constructed by an obvious greedy algorithm.
  \item
    Maximal matchings are non-unique; some may be preferable to others
    (e.g. choose heavy edges first).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Coarsening via maximal matching}

  \begin{center}
    \includegraphics{lec21mm.pdf}
  \end{center}

  \begin{itemize}
  \item Collapse nodes connected in matching into coarse nodes
  \item Add all edge weights between connected coarse nodes
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Software}

  All these use some flavor(s) of multilevel:
  \begin{itemize}
  \item METIS/ParMETIS (Kapyris)
  \item PARTY (U. Paderborn)
  \item Chaco (Sandia)
  \item Scotch (INRIA)
  \item Jostle (now commercialized)
  \item Zoltan (Sandia)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Is this it?}

  Consider partitioning for sparse matvec:
  \begin{itemize}
  \item Edge cuts $\neq$ communication volume
  \item Haven't looked at minimizing {\em maximum} communication volume
  \item Looked at communication volume -- what about latencies?
  \end{itemize}
  Some work beyond graph partitioning (e.g.~hypergraph in Zoltan).
\end{frame}

\begin{frame}
  \frametitle{Is this it?}

  Additional work on:
  \begin{itemize}
  \item Partitioning power law graphs
  \item Covering sets with small overlaps
  \end{itemize}
  Also: Classes of graphs with no small cuts (expanders)

\end{frame}


\end{document}

\documentclass{beamer}

\mode<presentation>
{
%  \usetheme[hideothersubsections]{PaloAlto}
  \usetheme{default}
  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

\usepackage{amsmath,amssymb}
\usepackage{times} 
\usepackage[T1]{fontenc}
\usepackage{tikz}

%\newrgbcolor{blue}{0 0 1}
%\newrgbcolor{red}{1 0 0}

\newcommand{\bbR}{\mathbb{R}}
\newcommand{\bbC}{\mathbb{C}}
\newcommand{\bfC}{\mathbf{C}}
\newcommand{\sfC}{\mathsf{C}}
\newcommand{\sfI}{\mathsf{I}}
\newcommand{\bfsigma}{\mathbf{\sigma}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\orth}{\mathrm{orth}}
\newcommand{\supp}{\mathrm{supp}}
\newcommand{\tr}{\mathrm{tr}}
\newcommand{\diag}{\mathrm{diag}}
\newcommand{\calF}{\mathcal{F}}
\newcommand{\calG}{\mathcal{G}}
\newcommand{\lambdamin}{\lambda_{\mathrm{min}}}
\newcommand{\lambdamax}{\lambda_{\mathrm{max}}}
\newcommand{\sigmamin}{\sigma_{\mathrm{min}}}
\newcommand{\sigmamax}{\sigma_{\mathrm{max}}}

\newcommand{\bfa}{\mathbf{a}}
\newcommand{\bfv}{\mathbf{v}}
\newcommand{\bfx}{\mathbf{x}}
\newcommand{\bff}{\mathbf{f}}

\newcommand{\myhref}[1]
%{\href{http://www.cs.berkeley.edu/~dbindel/present/movies/#1}}
{\href{run:/Users/dbindel/work/present/movies/#1}}

\title[CS 5220, Spring 2014]{Lecture 13: \\
  Memory models and SPH}

\author[]{David Bindel} \date[]{10 Mar 2014}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Logistics}

  \begin{itemize}
  \item HW 2 is due tonight
  \item HW 3 is posted, due April 1 (details today)
  \item Project proposals are also due April 1 (or sooner)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{On the topic of HW 2...}

  \begin{center}
  How would we parallelize {\tt wave1d} with OpenMP?
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{1D wave equation with OpenMP}

  \begin{enumerate}
  \item Just parallelize main loop?
    \begin{itemize}
    \item Certainly the easiest strategy -- efficiency?
    \item What scheduler would we use?
    \end{itemize}
  \item Treat almost like MPI code?
    \begin{itemize}
    \item How is information exchanged across states?
    \item What barriers are needed?
    \item Could we take multiple steps between barriers?
    \end{itemize}
  \end{enumerate}
\end{frame}


\begin{frame}
  \frametitle{Things to still think about with OpenMP}
  
  \begin{itemize}
  \item Proper serial performance tuning?
  \item Minimizing false sharing?
  \item Minimizing synchronization overhead?
  \item Minimizing loop scheduling overhead?
  \item Load balancing?
  \item Finding enough parallelism in the first place?
  \end{itemize}

  \vspace{1cm}
  Let's focus again on memory issues...
\end{frame}


\begin{frame}
  \frametitle{Memory model}

  \begin{itemize}
  \item Single processor: return last write
    \begin{itemize}
    \item What about DMA and memory-mapped I/O?
    \end{itemize}
  \item Simplest generalization: {\em sequential consistency} -- as if
    \begin{itemize}
    \item Each process runs in program order
    \item Instructions from different processes are interleaved
    \item Interleaved instructions ran on one processor
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Sequential consistency}

  \begin{quote}
    A multiprocessor is sequentially consistent if the result
    of any execution is the same as if the operations of all the
    processors were executed in some sequential order, and the
    operations of each individual processor appear in this sequence
    in the order specified by its program. \\
    \hfill -- Lamport, 1979
  \end{quote}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\vspace{5mm}
Without sequential consistency support, what if
\begin{enumerate}
\item Processor 2 caches {\tt flag}?
\item Compiler optimizes away loop?
\item Compiler reorders assignments on P1?
\end{enumerate}
Starts to look restrictive!

\end{frame}


\begin{frame}
  \frametitle{Sequential consistency: the good, the bad, the ugly}
  
  Program behavior is ``intuitive'':
  \begin{itemize}
  \item Nobody sees garbage values
  \item Time always moves forward
  \end{itemize}
  One issue is {\em cache coherence}:
  \begin{itemize}
  \item Coherence: different copies, same value
  \item Requires (nontrivial) hardware support
  \end{itemize}
  Also an issue for optimizing compiler!

  \vspace{1cm}
  There are cheaper {\em relaxed} consistency models.
\end{frame}


\begin{frame}
  \frametitle{Snoopy bus protocol}

  % Need picture
  Basic idea:
  \begin{itemize}
  \item Broadcast operations on memory bus
  \item Cache controllers ``snoop'' on all bus transactions
    \begin{itemize}
    \item Memory writes induce serial order
    \item Act to enforce coherence (invalidate, update, etc)
    \end{itemize}
  \end{itemize}

  Problems:
  \begin{itemize}
  \item Bus bandwidth limits scaling
  \item Contending writes are slow
  \end{itemize}

  \vspace{5mm}
  There are other protocol options (e.g. directory-based). \\
  But usually give up on {\em full} sequential consistency.

\end{frame}


\begin{frame}
  \frametitle{Weakening sequential consistency}

  Try to reduce to the {\em true} cost of sharing
  \begin{itemize}
  \item {\tt volatile} tells compiler when to worry about sharing
  \item Memory fences tell when to force consistency
  \item Synchronization primitives (lock/unlock) include fences
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Sharing}
  
  True sharing:
  \begin{itemize}
  \item Frequent writes cause a bottleneck.
  \item Idea: make independent copies (if possible).
  \item Example problem: malloc/free data structure.
  \end{itemize}
  
  \vspace{5mm}
  False sharing:
  \begin{itemize}
  \item Distinct variables on same cache block
  \item Idea: make processor memory contiguous (if possible)
  \item Example problem: array of ints, one per processor
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Take-home message}

  \begin{itemize}
  \item Sequentially consistent shared memory is a useful idea...
    \begin{itemize}
    \item ``Natural'' analogue to serial case
    \item Architects work hard to support it
    \end{itemize}
  \item ... but implementation is costly!
    \begin{itemize}
    \item Makes life hard for optimizing compilers
    \item Coherence traffic slows things down
    \item Helps to limit sharing
    \end{itemize}
  \end{itemize}
  \vspace{1cm}
  Have to think about these things to get good performance.

\end{frame}


\begin{frame}
  \frametitle{Your next mission!}

  \begin{center}
    \includegraphics[width=\textwidth]{hw3_output.png}
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Smoothed Particle Hydrodynamics (SPH)}

  \begin{itemize}
  \item Particle based method for fluid simulation
    \begin{itemize}
    \item Representative of other particle-based methods
    \item More visually interesting than MD with Lennard-Jones?
    \end{itemize}
  \item 
    Particle $i$ (a fluid blob) evolves according to
    \[
    m \bfa_i = \sum_{|\bfx_j-\bfx_i| \leq h} \bff_{ij}
    \]
    where force law satisfies $\bff_{ij} = -\bff_{ji}$.
  \item
    Chief performance challenge: fast evaluation of forces!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Task 1: Binning / spatial hashing}

  \begin{itemize}
  \item Partition space into bins of size $\geq h$ (interaction
    radius)
  \item Only check for interactions in nearby bins
  \item Trade off between bin size, number of interaction checks
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Task 1: Binning / spatial hashing}

  \begin{center}
    \input binfig
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Task 1: Binning / spatial hashing}

  \begin{center}
    \input binfig2
  \end{center}

  \begin{itemize}
  \item Keep particles in an array as usual
  \item Also keep array of head pointers for bins
  \item Thread linked list structures for bin contents
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Task 1: Binning / spatial hashing}

  \begin{itemize}
  \item Typical situation: lots of empty bins
  \item Empty boxes take up space!
  \item Alternative: spatial hashing
    \begin{itemize}
    \item Map multiple bins to one storage location
    \item Avoid {\em collisions} (several bins map to same place)
    \item Maybe preserve locality?
    \end{itemize}
  \item Idea: Figure out potential neighbors good for a few steps?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Ordering}

  \begin{itemize}
  \item Bins naturally identified with two or three indices
  \item Want to map to a single index
    \begin{itemize}
    \item To serve as a hash key
    \item For ordering computations
    \end{itemize}
  \item Row/column major: mediocre locality
  \item Better idea: Z-Morton ordering
    \begin{itemize}
    \item Interleave bits of $(x,y,z)$ indices
    \item Efficient construction a little fiddly
    \item But basic picture is simple!
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Z Morton ordering}

  \begin{center}
    \input zmorton

    Equivalent to height-balanced quadtree / octtree.
  \end{center}
\end{frame}


\begin{frame}
  \frametitle{Task 2: Profiling}

  \begin{itemize}
  \item Computation involves several different steps
    \begin{itemize}
    \item Finding nearest neighbors
    \item Computing local densities
    \item Computing local pressure / viscous forces
    \item Time stepping
    \item I/O
    \end{itemize}
  \item Want to act based on timing data
    \begin{itemize}
    \item Manual instrumentation?
    \item Profilers?
    \end{itemize}
  \item Which takes the most time? Fix that first.
  \item Consider both algorithm improvements and tuning.
  \item Lecture Thursday will be (at least partly) profiling.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Task 3: Parallelization}

  Two major issues:
  \begin{itemize}
  \item How do we decompose the problem?
    \begin{itemize}
    \item Processors own regions in space?
    \item Processors own fixed sets of particles?
    \item Processors own sets of possible force interactions?
    \item Hybrids recommended!
    \end{itemize}
  \item How do we synchronize force computations?
    \begin{itemize}
    \item Note: Compute $\bff_{ij}$ and $\bff_{ji}$ simultaneously now
    \item Could keep multiple updates per processor and reduce?
    \item Could use multi-color techniques \\
      (no two processors handle neighbors concurrently)?
    \item Interacts with the problem decomposition!
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example approach: multi-color ordering}
  \begin{center}
    \input zmortonc

    No blue cell neighbors another blue.
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Things to think about}

  \begin{itemize}
  \item How do we make sure we don't break the code?
  \item How fast is parallel code for $p=1$?
  \item Are there load balance issues?
  \item Do we synchronize frequently (many times per step)?
  \item Do we get good strong scaling?
  \item Do we get good weak scaling?
    \begin{itemize}
    \item Note: Smaller particles $\implies$ smaller time step needed!
    \item Have to be careful to compare apples to apples
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Task 4: Play}

  \begin{itemize}
  \item How fast can we make the serial code?
  \item How should we improve the initialization?
  \item Could we time step more intelligently?
  \item What about surface tension (see M\"uller et al)?
  \item What about better boundary conditions?
  \item What about swishing, pouring, etc?
  \item What about recent improvements for incompressible flow?
  \end{itemize}

\end{frame}

\end{document}

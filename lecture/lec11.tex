\documentclass{beamer}

\mode<presentation>
{
%  \usetheme[hideothersubsections]{PaloAlto}
  \usetheme{default}
  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{arrows,automata}
\pgfplotsset{compat=1.9}

\usepackage{amsmath,amssymb}
\usepackage{times} 
\usepackage[T1]{fontenc}

%\newrgbcolor{blue}{0 0 1}
%\newrgbcolor{red}{1 0 0}

\newcommand{\bbR}{\mathbb{R}}
\newcommand{\bbC}{\mathbb{C}}
\newcommand{\bfC}{\mathbf{C}}
\newcommand{\sfC}{\mathsf{C}}
\newcommand{\sfI}{\mathsf{I}}
\newcommand{\bfsigma}{\mathbf{\sigma}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\orth}{\mathrm{orth}}
\newcommand{\supp}{\mathrm{supp}}
\newcommand{\tr}{\mathrm{tr}}
\newcommand{\diag}{\mathrm{diag}}
\newcommand{\calF}{\mathcal{F}}
\newcommand{\calG}{\mathcal{G}}
\newcommand{\lambdamin}{\lambda_{\mathrm{min}}}
\newcommand{\lambdamax}{\lambda_{\mathrm{max}}}
\newcommand{\sigmamin}{\sigma_{\mathrm{min}}}
\newcommand{\sigmamax}{\sigma_{\mathrm{max}}}

\newcommand{\myhref}[1]
%{\href{http://www.cs.berkeley.edu/~dbindel/present/movies/#1}}
{\href{run:/Users/dbindel/work/present/movies/#1}}

\title[CS 5220, Fall 2011]{Lecture 6: \\
  Intro to shared memory programming}

\author[]{David Bindel} \date[]{15 Sep 2011}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Logistics}

  \begin{itemize}
  \item HW 3: Out soon!
  \item For HW 2:
    \begin{itemize}
    \item Basic idea should be easy ...
    \item ... if you don't get stuck on logistics
    \item ... which can easily happen
    \item Please don't procrastinate.
    \end{itemize}
  \item Final project proposal
    \begin{itemize}
    \item Will post a little CMS entry soon
    \item Use Piazza to advertise ideas / find partners
    \end{itemize}
  \item Final project goals
    \begin{itemize}
    \item Goal 1: Do some performance analysis/tuning
    \item Goal 2: Work on something you care about
    \item Goal 3: Get someone else interested!  (groups 1--4)
    \item Output: Preliminary presentation and final write-up
    \end{itemize}
  \end{itemize}
\end{frame}

% =========

\begin{frame}
  \frametitle{Reminder: 1D wave}

  \begin{tikzpicture}[scale=0.8]
    \draw[fill=red] (0,0) rectangle (2,1);
    \draw[fill=blue!80] (2,0) rectangle (4,1);
    \draw (-1,0) grid (5,1);
    \node[above left] at (-1,0) {Logical mesh};

    \draw (-2,-0.5)--(6,-0.5);

    \draw[fill=red] (0,-2) rectangle (2,-1);
    \draw[fill=blue!30] (2,-2) rectangle (3,-1);
    \draw (-1,-2) grid (3,-1);
    \node[above left] at (-1,-2) {P0 array};

    \draw[fill=red!30] (1,-4) rectangle (2,-3);
    \draw[fill=blue!80] (2,-4) rectangle (4,-3);
    \draw (1,-4) grid (5,-3);
    \node[above right] at (5,-4) {P1 array};

    \draw[thick,->] (1.5,-2.8) -- (1.5,-2.2);
    \draw[thick,->] (2.5,-2.2) -- (2.5,-2.8);
  \end{tikzpicture}

  \vspace{5mm}
  Common message passing pattern
  \begin{itemize}
  \item Logical {\em global} structure
  \item {\em Local} representation per processor
  \item Local data may have redundancy
    \begin{itemize}
    \item Example: Data in ghost cells
    \item Example: Replicated book-keeping data ({\tt pidx} in our code)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Message passing pain}

  Common message passing pattern
  \begin{itemize}
  \item Logical {\em global} structure
  \item {\em Local} representation per processor
  \item Local data may have redundancy
    \begin{itemize}
    \item Example: Data in ghost cells
    \item Example: Replicated book-keeping data ({\tt pidx} in our code)
    \end{itemize}
  \end{itemize}

  \vspace{5mm}
  Big pain point:
  \begin{itemize}
  \item Thinking about many partly-overlapping representations
  \item Maintaining consistent picture across processes
  \end{itemize}

  \vspace{5mm}
  Wouldn't it be nice to have just one representation?
\end{frame}

\begin{frame}
  \frametitle{Shared memory vs message passing}

  \begin{itemize}
  \item Implicit communication via memory vs explicit messages
  \item Still need separate global vs local picture?
    \begin{itemize}
    \item {\bf No:} One thread-safe data structure may be easier
    \item {\bf Yes:} More sharing can hurt performance
      \begin{itemize}
      \item Synchronization costs cycles even with no contention
      \item Contention for locks reduces parallelism
      \item Cache coherency can slow even non-contending access
      \end{itemize}
    \end{itemize}
  \item ``Easy'' approach: add multi-threading to serial code
  \item Better performance: design like a message-passing code
  \end{itemize}
\end{frame}

% =========

\begin{frame}
  \frametitle{Reminder: Shared memory programming model}
  
  Program consists of {\em threads} of control.
  \begin{itemize}
  \item Can be created dynamically
  \item Each has private variables (e.g.~local)
  \item Each has shared variables (e.g.~heap)
  \item Communication through shared variables
  \item Coordinate by synchronizing on variables
  \item Examples: pthreads, OpenMP, Cilk, Java threads
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Mechanisms for thread birth/death}

  \begin{itemize}
  \item Statically allocate threads at start
  \item Fork/join (pthreads)
  \item Fork detached threads (pthreads)
  \item Cobegin/coend (OpenMP?)
    \begin{itemize}
    \item Like fork/join, but lexically scoped
    \end{itemize}
  \item Futures
    \begin{itemize}
    \item {\tt v = future(somefun(x))}
    \item Attempts to use {\tt v} wait on evaluation
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Mechanisms for synchronization}

  \begin{itemize}
  \item Locks/mutexes (enforce mutual exclusion)
  \item Monitors (like locks with lexical scoping)
  \item Barriers
  \item Condition variables (notification)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Concrete code: pthreads}

  \begin{itemize}
  \item pthreads = POSIX threads
  \item Standardized across UNIX family
  \item Fairly low-level
  \item Heavy weight?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Wait, what's a thread?}

  Processes have {\em state}.  Threads share some:
  \begin{itemize}
  \item Instruction pointer (per thread)
  \item Register file (per thread)
  \item Call stack (per thread)
  \item Heap memory (shared)
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Thread birth and death}
  
  \begin{center}
    \includegraphics[width=0.8\textwidth]{forkjoin.pdf}
  \end{center}
  Thread is created by {\em forking}. \\
  When done, {\em join} original thread.
\end{frame}


\begin{frame}[fragile]
  \frametitle{Thread birth and death}
  
\begin{verbatim}
void thread_fun(void* arg);

pthread_t thread_id;
pthread_create(&thread_id, &thread_attr, 
               thread_fun, &fun_arg);
...
pthread_join(&thread_id, NULL);
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Mutex}

  \begin{center}
    \includegraphics[width=\textwidth]{mutex.pdf}
  \end{center}

  Allow only one process at a time in {\em critical section} (red). \\
  Synchronize using locks, aka mutexes ({\em mutual exclusion vars}).

\end{frame}


\begin{frame}[fragile]
  \frametitle{Mutex}

\begin{verbatim}
pthread_mutex_t l;
pthread_mutex_init(&l, NULL);
...
pthread_mutex_lock(&l);
/* Critical section here */
pthread_mutex_unlock(&l);
...
pthread_mutex_destroy(&l);
\end{verbatim}

\end{frame}


\begin{frame}
  \frametitle{Condition variables}

  \begin{center}
    \includegraphics[width=\textwidth]{condvar.pdf}
  \end{center}

  \vspace{5mm}
  Allow thread to wait until condition holds
  (e.g. work available).
\end{frame}


\begin{frame}[fragile]
  \frametitle{Condition variables}

\hspace{0.15\textwidth}
\begin{minipage}{0.45\textwidth}
\begin{verbatim}
pthread_mutex_t l;
pthread_cond_t cv;
pthread_mutex_init(&l)
pthread_cond_init(&cv, NULL);
\end{verbatim}
\vspace{1mm}
\end{minipage} \\
\begin{minipage}{0.45\textwidth}
\begin{verbatim}
/* Thread 0 */
mutex_lock(&l);
add_work();
cond_signal(&cv);
mutex_unlock(&l);

\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\begin{verbatim}
/* Thread 1 */
mutex_lock(&l);
if (!work_ready)
    cond_wait(&cv, &l);
get_work();
mutex_unlock();
\end{verbatim}
\end{minipage} \\
\hspace{0.15\textwidth}
\begin{minipage}{0.45\textwidth}
\vspace{5mm}
\begin{verbatim}
pthread_cond_destroy(&cv);
pthread_mutex_destroy(&l);
\end{verbatim}
\end{minipage}

\end{frame}


\begin{frame}
  \frametitle{Barriers}

  \begin{center}
    \includegraphics[width=\textwidth]{barrier.pdf}
  \end{center}
  
  \vspace{1cm}
  Computation phases separated by barriers. \\
  Everyone reaches the barrier, then proceeds.
  
\end{frame}


\begin{frame}[fragile]
  \frametitle{Barriers}

\begin{verbatim}
pthread_barrier_t b;
pthread_barrier_init(&b, NULL, nthreads);
...
pthread_barrier_wait(&b);
...
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Synchronization pitfalls}

  \begin{itemize}
  \item Incorrect synchronization $\implies$ {\em deadlock}
    \begin{itemize}
    \item All threads waiting for what the others have
    \item Doesn't always happen! $\implies$ hard to debug
    \end{itemize}
  \item Too little synchronization $\implies$ data races
    \begin{itemize}
    \item Again, doesn't always happen!
    \end{itemize}
  \item Too much synchronization  $\implies$ poor performance
    \begin{itemize}
    \item ... but makes it easier to think through correctness
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Deadlock}

  \begin{minipage}{0.45\textwidth}
    \vspace{2mm}
    Thread 0:  \vspace{2mm} \\
    lock(l1); lock(l2); \\
    Do something \\
    unlock(l2); unlock(l1); 
    \vspace{2mm}
  \end{minipage}
  \begin{minipage}{0.45\textwidth}
    \vspace{2mm}
    Thread 1: \vspace{2mm} \\
    lock(l2); lock(l1); \\
    Do something \\
    unlock(l1); unlock(l2); 
    \vspace{2mm}
  \end{minipage}
  
  \vspace{5mm}
  \hspace{0.2\textwidth}
  \begin{minipage}{0.5\textwidth}
  Conditions:
  \begin{enumerate}
  \item Mutual exclusion
  \item Hold and wait
  \item No preemption
  \item Circular wait
  \end{enumerate}
  \end{minipage}

\end{frame}


\begin{frame}
  \frametitle{The problem with pthreads}

  Portable standard, but...
  \begin{itemize}
  \item Low-level library standard
  \item Verbose
  \item Makes it easy to goof on synchronization
  \item Compiler doesn't help out much
  \end{itemize}
  OpenMP is a common alternative.
  
\end{frame}


\begin{frame}
  \frametitle{Example: Work queues}

  \begin{itemize}
  \item Job composed of different tasks
  \item Work gang of threads to execute tasks
  \item Maybe tasks can be added over time?
  \item Want dynamic load balance
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Example: Work queues}

  Basic data:
  \begin{itemize}
  \item Gang of threads
  \item Work queue data structure
  \item Mutex protecting data structure
  \item Condition to signal work available
  \item Flag to indicate all done?
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Work queues}

\begin{verbatim}
task_t get_task() {
  task_t result;
  pthread_mutex_lock(&task_l);
  if (done_flag) {
    pthread_mutex_unlock(&task_l);
    pthread_exit(NULL);
  }
  if (num_tasks == 0)
    pthread_cond_wait(&task_ready, &task_l);
  ... Remove task from data struct ...
  pthread_mutex_unlock(&task_l);
  return result;
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Work queues}

\begin{verbatim}
void add_task(task_t task) {
  pthread_mutex_lock(&task_l);
  ... Add task to data struct ...
  if (num_tasks++ == 0)
    pthread_cond_signal(&task_ready);
  pthread_mutex_unlock(&task_l);
}
\end{verbatim}
\end{frame}


\begin{frame}
  \frametitle{Monte Carlo}
  
  Basic idea:  Express answer $a$ as
  \[
    a = E[f(X)]
  \]
  for some random variable(s) $X$.

  \vspace{1cm}
  Typical toy example:
  \[
    \pi/4 = E[\chi_{[0,1]}(X^2 + Y^2)] \mbox{ where } X, Y \sim U(-1,1).
  \]
  We'll be slightly more interesting...

\end{frame}


\begin{frame}
  \frametitle{A toy problem}

  Given ten points $(X_i, Y_i)$ drawn uniformly in $[0,1]^2$,
  what is the expected minimum distance between any pair?
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 1}

Serial version:
\begin{verbatim}
sum_fX = 0;
for i = 1:ntrials
  x = rand(10,2);
  fX = min distance between points in x;
  sum_fX = sum_fX + fx;
end
result = sum_fX/ntrials;
\end{verbatim}
Parallel version: run twice and average results?! \\
No communication --- {\em embarrassingly parallel}

\vspace{5mm}
Need to worry a bit about {\tt rand}...

\end{frame}


\begin{frame}
  \frametitle{Error estimators}

  Central limit theorem: if $R$ is computed result, then
  \[
    R \sim N\left( E[f(X)], \frac{\sigma_{f(X)}}{\sqrt{n}} \right).
  \]
  So:
  \begin{itemize}
  \item
    Compute sample standard deviation $\hat{\sigma_{f(X)}}$
  \item
    Error bars are $\pm \hat{\sigma_{f(X)}}/\sqrt{n}$
  \item
    Use error bars to monitor convergence
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2}

Serial version:
\begin{verbatim}
sum_fX = 0;
sum_fX2 = 0;
for i = 1:ntrials
  x  = rand(10,2);
  fX = min distance between points in x;
  sum_fX  = sum_fX + fX;
  sum_fX2 = sum_fX + fX*fX;
  result = sum_fX/i;
  errbar = sqrt(sum_fX2-sum_fX*sum_fX/i)/i;
  if (abs(errbar/result) < reltol), break; end
end
result = sum_fX/ntrials;
\end{verbatim}
Parallel version: ?

\end{frame}


\begin{frame}
  \frametitle{Pondering parallelism}

  Two major points:
  \begin{itemize}
  \item How should we handle random number generation?
  \item How should we manage termination criteria?
  \end{itemize}

  Some additional points (briefly):
  \begin{itemize}
  \item How quickly can we compute {\tt fX}?
  \item Can we accelerate convergence (variance reduction)?
  \end{itemize}

\end{frame}

\end{document}

%% -- Stopping point from last time -- 

\begin{frame}
  \frametitle{Pseudo-random number generation}

  \begin{itemize}
  \item Pretend {\em deterministic} and process is random. \\
    $\implies$ We lose if it doesn't {\em look} random!
  \item RNG functions have {\em state} \\
    $\implies$ Basic {\tt random()} call is {\em not} thread-safe!
  \item Parallel strategies:
    \begin{itemize}
    \item Put RNG in critical section (slow)
    \item Run independent RNGs per thread
      \begin{itemize}
      \item Concern: correlation between streams
      \end{itemize}
    \item Split stream from one RNG
      \begin{itemize}
      \item E.g. thread 0 uses even steps, thread 1 uses odd steps
      \item Helpful if it's cheap to skip steps!
      \end{itemize}
    \end{itemize}
  \item
    Good libraries help!  Mersenne twister, SPRNG, ...?
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{One solution}

  \begin{itemize}
  \item Use a version of Mersenne twister with no global state:
\begin{verbatim}
void sgenrand(long seed, 
              struct mt19937p* mt);
double genrand(struct mt19937p* mt);
\end{verbatim}
  \item Choose pseudo-random seeds per thread at startup:
\begin{verbatim}
long seeds[NTHREADS];
srandom(clock());
for (i = 0; i < NTHREADS; ++i)
    seeds[i] = random();
...
/* sgenrand(seeds[i], mt) for thread i */
\end{verbatim}
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.1p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0;
for each thread in parallel
  do
    fX = result of one random trial
    ++n;
    sum_fX += fX;
    sum_fX2 += fX*fX;
    errbar = ...
    if (abs(errbar/result) < reltol), break; end
  loop
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.2p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0; done = false;
for each thread in parallel
  do
    fX = result of one random trial
    get lock
      ++n;
      sum_fX = sum_fX + fX;
      sum_fX2 = sum_fX2 + fX*fX;
      errbar = ...
      if (abs(errbar/result) < reltol)
        done = true; 
      end
    release lock
  until done
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.3p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0; done = false;
for each thread in parallel
  do
    batch_sum_fX, batch_sum_fX2 = B trials
    get lock
      n += B;
      sum_fX += batch_sum_fX;
      sum_fX2 += batch_sum_fX2;
      errbar = ...
      if (abs(errbar/result) < reltol)
        done = true; 
      end
    release lock
  until done or n > n_max
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}
  \frametitle{Toy problem: actual code (pthreads)}
  
\end{frame}


\begin{frame}
  \frametitle{Some loose ends}

  \begin{itemize}
  \item Alternative: ``master-slave'' organization
    \begin{itemize}
    \item Master sends out batches of work to slaves
    \item Example: SETI at Home, Folding at Home, ...
    \end{itemize}
  \item What is the right batch size?
    \begin{itemize}
    \item Large $B$ $\implies$ amortize locking/communication overhead \\
          (and variance actually helps with contention!)
    \item Small $B$ avoids too much extra work
    \end{itemize}
  \item How to evaluate $f(X)$?
    \begin{itemize}
    \item For $p$ points, obvious algorithm is $O(p^2)$
    \item Binning points better?  No gain for $p$ small...
    \end{itemize}
  \item Is $f(X)$ the right thing to evaluate?
    \begin{itemize}
    \item Maybe $E[g(X)] = E[f(X)]$ but 
          $\mathrm{Var}[g(X)] \ll \mathrm{Var}[f(X)]$?
    \item May make much more difference than parallelism!
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{The problem with pthreads revisited}
  
  pthreads can be painful!
  \begin{itemize}
  \item Makes code verbose
  \item Synchronization is hard to think about
  \end{itemize}
  
  \vspace{5mm}
  Would like to make this more automatic!
  \begin{itemize}
  \item ... and have been trying for a couple decades.
  \item OpenMP gets us {\em part} of the way
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{OpenMP: Open spec for MultiProcessing}

  \begin{itemize}
  \item Standard API for multi-threaded code
    \begin{itemize}
    \item Only a spec --- multiple implementations
    \item Lightweight syntax
    \item C or Fortran (with appropriate compiler support)
    \end{itemize}
  \item High level:
    \begin{itemize}
    \item Preprocessor/compiler directives (80\%)
    \item Library calls (19\%)
    \item Environment variables (1\%)
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel ``hello world''}

\begin{verbatim}
#include <stdio.h>
#include <omp.h>

int main()
{
    #pragma omp parallel
    printf("Hello world from %d\n", 
           omp_get_thread_num());

    return 0;
}
\end{verbatim}
\end{frame}


\begin{frame}
  \frametitle{Parallel sections}

\begin{center}
\includegraphics[width=0.8\textwidth]{ompsec.pdf}
\end{center}
\begin{itemize}
\item Basic model: fork-join 
\item Each thread runs same code block
\item Annotations distinguish shared ($s$) and private ($i$) data
\item {\em Relaxed consistency} for shared data
\end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel sections}

\begin{center}
\includegraphics[width=0.8\textwidth]{ompsec.pdf}
\end{center}
\begin{verbatim}
...
double s[MAX_THREADS];
int i;
#pragma omp parallel shared(s) private(i)
{
  i = omp_get_thread_num();
  s[i] = i;
}
...
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Critical sections}

  \begin{center}
    \includegraphics[width=\textwidth]{mutex.pdf}
  \end{center}

  \begin{itemize}
  \item Automatically lock/unlock at ends of {\em critical section}
  \item Automatically memory flushes for consistency
  \item Locks are still there if you really need them...
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Critical sections}

  \begin{center}
    \includegraphics[width=\textwidth]{mutex.pdf}
  \end{center}

\begin{verbatim}
#pragma omp parallel {
  ...
  #pragma omp critical my_data_cs
  {
    ... modify data structure here ...
  }
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
\frametitle{Barriers}

  \begin{center}
    \includegraphics[width=\textwidth]{barrier.pdf}
  \end{center}

\begin{verbatim}
#pragma omp parallel
for (i = 0; i < nsteps; ++i) {
  do_stuff
  #pragma omp barrier
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: actual code (OpenMP)}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: actual code (OpenMP)}

  A practical aside...
  \begin{itemize}
  \item GCC 4.3+ has OpenMP support by default
    \begin{itemize}
    \item Earlier versions may support (e.g. latest Xcode {\tt gcc-4.2})
    \item GCC 4.4 (prerelease) for my laptop has buggy support!
    \item {\tt -O3 -fopenmp} == death of an afternoon
    \end{itemize}
  \item Need {\tt -fopenmp} for both compile and link lines
\begin{verbatim}
gcc -c -fopenmp foo.c
gcc -o -fopenmp mycode.x foo.o
\end{verbatim}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Parallel loops}

\begin{center}
\includegraphics[width=\textwidth]{omploop.pdf}
\end{center}

\begin{itemize}
\item
  Independent loop body? At least order doesn't matter%
\footnote{If order matters, there's an {\tt ordered} modifier.}.
\item
  Partition index space among threads
\item 
  Implicit barrier at end (except with {\tt nowait})
\end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel loops}

\begin{verbatim}
/* Compute dot of x and y of length n */
int i, tid;
double my_dot, dot = 0;
#pragma omp parallel \
        shared(dot,x,y,n) \
        private(i,my_dot)
{
  tid = omp_get_thread_num();
  my_dot = 0;
 
  #pragma omp for
  for (i = 0; i < n; ++i)
    my_dot += x[i]*y[i];

  #pragma omp critical
  dot += my_dot;
}
\end{verbatim}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel loops}

\begin{verbatim}
/* Compute dot of x and y of length n */
int i, tid;
double dot = 0;
#pragma omp parallel \
        shared(x,y,n) \
        private(i) \
        reduction(+:dot)
{
  #pragma omp for
  for (i = 0; i < n; ++i)
    dot += x[i]*y[i];
}
\end{verbatim}

\end{frame}


\begin{frame}
  \frametitle{Parallel loop scheduling}
  
  Partition index space different ways:
  \begin{itemize}
  \item {\tt static[(chunk)]}: decide at start of loop;
    default chunk is {\tt n/nthreads}.  Lowest overhead,
    most potential load imbalance.
  \item {\tt dynamic[(chunk)]}: each thread takes {\tt chunk}
    iterations when it has time; default {\tt chunk} is 1.
    Higher overhead, but automatically balances load.
  \item {\tt guided}: take chunks of size unassigned iterations/threads;
    chunks get smaller toward end of loop.  Somewhere between
    {\tt static} and {\tt dynamic}.
  \item {\tt auto}: up to the system!
  \end{itemize}
  Default behavior is implementation-dependent.

\end{frame}


\begin{frame}
  \frametitle{Other parallel work divisions}
  
  \begin{itemize}
  \item {\tt single}: do only in one thread (e.g. I/O)
  \item {\tt master}: do only in one thread; others skip
  \item {\tt sections}: like cobegin/coend
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Essential complexity?}
  
  Fred Brooks ({\em Mythical Man Month}) identified two types
  of software complexity: essential and accidental.

  \vspace{5mm}
  Does OpenMP address accidental complexity?  Yes, somewhat!

  \vspace{5mm}
  Essential complexity is harder.
\end{frame}


\begin{frame}
  \frametitle{Things to still think about with OpenMP}
  
  \begin{itemize}
  \item Proper serial performance tuning?
  \item Minimizing false sharing?
  \item Minimizing synchronization overhead?
  \item Minimizing loop scheduling overhead?
  \item Load balancing?
  \item Finding enough parallelism in the first place?
  \end{itemize}

  \vspace{1cm}
  Let's focus again on memory issues...
\end{frame}


\begin{frame}
  \frametitle{Memory model}

  \begin{itemize}
  \item Single processor: return last write
    \begin{itemize}
    \item What about DMA and memory-mapped I/O?
    \end{itemize}
  \item Simplest generalization: {\em sequential consistency} -- as if
    \begin{itemize}
    \item Each process runs in program order
    \item Instructions from different processes are interleaved
    \item Interleaved instructions ran on one processor
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Sequential consistency}

  \begin{quote}
    A multiprocessor is sequentially consistent if the result
    of any execution is the same as if the operations of all the
    processors were executed in some sequential order, and the
    operations of each individual processor appear in this sequence
    in the order specified by its program. \\
    \hfill -- Lamport, 1979
  \end{quote}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\vspace{5mm}
Without sequential consistency support, what if
\begin{enumerate}
\item Processor 2 caches {\tt flag}?
\item Compiler optimizes away loop?
\item Compiler reorders assignments on P1?
\end{enumerate}
Starts to look restrictive!

\end{frame}


\begin{frame}
  \frametitle{Sequential consistency: the good, the bad, the ugly}
  
  Program behavior is ``intuitive'':
  \begin{itemize}
  \item Nobody sees garbage values
  \item Time always moves forward
  \end{itemize}
  One issue is {\em cache coherence}:
  \begin{itemize}
  \item Coherence: different copies, same value
  \item Requires (nontrivial) hardware support
  \end{itemize}
  Also an issue for optimizing compiler!

  \vspace{1cm}
  There are cheaper {\em relaxed} consistency models.
\end{frame}


\begin{frame}
  \frametitle{Snoopy bus protocol}

  % Need picture
  Basic idea:
  \begin{itemize}
  \item Broadcast operations on memory bus
  \item Cache controllers ``snoop'' on all bus transactions
    \begin{itemize}
    \item Memory writes induce serial order
    \item Act to enforce coherence (invalidate, update, etc)
    \end{itemize}
  \end{itemize}

  Problems:
  \begin{itemize}
  \item Bus bandwidth limits scaling
  \item Contending writes are slow
  \end{itemize}

  \vspace{5mm}
  There are other protocol options (e.g. directory-based). \\
  But usually give up on {\em full} sequential consistency.

\end{frame}


\begin{frame}
  \frametitle{Weakening sequential consistency}

  Try to reduce to the {\em true} cost of sharing
  \begin{itemize}
  \item {\tt volatile} tells compiler when to worry about sharing
  \item Memory fences tell when to force consistency
  \item Synchronization primitives (lock/unlock) include fences
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Sharing}
  
  True sharing:
  \begin{itemize}
  \item Frequent writes cause a bottleneck.
  \item Idea: make independent copies (if possible).
  \item Example problem: malloc/free data structure.
  \end{itemize}
  
  \vspace{5mm}
  False sharing:
  \begin{itemize}
  \item Distinct variables on same cache block
  \item Idea: make processor memory contiguous (if possible)
  \item Example problem: array of ints, one per processor
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Take-home message}

  \begin{itemize}
  \item Sequentially consistent shared memory is a useful idea...
    \begin{itemize}
    \item ``Natural'' analogue to serial case
    \item Architects work hard to support it
    \end{itemize}
  \item ... but implementation is costly!
    \begin{itemize}
    \item Makes life hard for optimizing compilers
    \item Coherence traffic slows things down
    \item Helps to limit sharing
    \end{itemize}
  \end{itemize}
  \vspace{1cm}
  Have to think about these things to get good performance.

\end{frame}


\end{document}

function tikz_spy(fp, A, k)

  % File name handling
  local_file = 0;
  if ischar(fp)
    local_file = 1;
    fp = fopen(fp, 'w');
  end

  % Default to no offset
  if nargin < 3, k = 0; end

  % Box the whole matrix
  [m,n] = size(A);
  tikz_box(fp, A, [1,m],  [1,n],  k, 'ultra thick,rounded corners');

  % Plot the entries
  for i = 1:m
    for j = 1:n
      xx = tikz_pt(A,i,j, k);
      if A(i,j)
        fprintf(fp,'\\draw[fill=blue] (%.2fpt,%.2fpt) circle [radius=1.4pt]; \n', xx);
      end
    end
  end

  % Clean up
  if local_file, fclose(fp); end

function xx = tikz_pt(A, i,j, k)

  [m,n] = size(A);
  xx(1) = (k+j-1)*4.8;
  xx(2) = (m-i)*4.8;

function tikz_box(fp, A, ii, jj, k, features)

  fprintf(fp, '\\draw[%s] (%.2fpt,%.2fpt) rectangle (%.2fpt,%.2fpt);\n', ...
          features, ...
          tikz_pt(A,ii(1)-0.5,jj(1)-0.5,k), ...
          tikz_pt(A,ii(2)+0.5,jj(2)+0.5,k));



\documentclass{beamer}

\mode<presentation>
{
%  \usetheme[hideothersubsections]{PaloAlto}
  \usetheme{default}
  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

\usepackage{amsmath,amssymb}
%\usepackage{times} 
\usepackage[T1]{fontenc}

%\usetheme{Berlin}

\newcommand{\bfv}{\mathbf{v}}
\newcommand{\bfa}{\mathbf{a}}
\newcommand{\bff}{\mathbf{f}}
\newcommand{\bfg}{\mathbf{g}}
\newcommand{\bft}{\mathbf{t}}

\newcommand{\bfx}{\mathbf{x}}
\newcommand{\bfy}{\mathbf{y}}
\newcommand{\bfF}{\mathbf{F}}
\newcommand{\bfr}{\mathbf{r}}
\newcommand{\bfn}{\mathbf{n}}
\newcommand{\bfI}{\mathbf{I}}
\newcommand{\bbR}{\mathbb{R}}
\newcommand{\fl}{\operatorname{fl}}
\newcommand{\macheps}{\epsilon_{\mathrm{machine}}}

\title[CS 5220, Spring 2014]{Lecture 20: \\
  The impact of floating point}

\author[]{David Bindel} \date[]{10 Apr 2011}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Logistics}

  \begin{itemize}
  \item SPH
    \begin{itemize}
    \item You have until midnight Friday
    \item Prioritize understanding performance to code fiddling
    \item Though performance of broken code is not so interesting
    \end{itemize}
  \item Topic modeling
    \begin{itemize}
    \item You have until May 2 -- finish early!
    \item Just upgraded Julia on C4.
    \end{itemize}
  \item Final project also on the horizon!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Lessons from SPH}
  
  \begin{itemize}
  \item Smart algorithms and tuning trump tuning alone
    \begin{itemize}
    \item Binning does far more for performance than parallelism.
    \end{itemize}
  \item There is no point in tuning non-bottlenecks
    \begin{itemize}
    \item The leapfrog updates take effectively no time
    \item Re-binning is also negligible compared to interactions
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Lessons from SPH}

  \begin{itemize}
  \item OpenMP is not magic
    \begin{itemize}
    \item Starting/stopping thread times is expensive
    \item Synchronization is expensive
    \item Easy to slow things down by parallelizing!
    \item Helps to be thoroughly aware of dependency structure
    \end{itemize}
  \item Race conditions are sometimes soul-suckingly subtle
    \begin{itemize}
    \item May not show up except by accidents of timing
    \item Very difficult to detect (note: {\tt valgrind} DRD tool may help)
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Lessons from SPH}

  \begin{itemize}
  \item Test and debug incrementally
    \begin{itemize}
    \item The code and pray method does not scale
    \item Automate the process of sanity checking your computations!
    \end{itemize}
  \item Pointer chasing (and index chasing) are performance drags
    \begin{itemize}
    \item Pays to have data locality and simple, regular access patterns
    \item Can get significant wins from paying attention to data used
      in inner loops
    \end{itemize}
  \item {\tt calloc} and {\tt free} are not free
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Performance koans}
  
  \begin{itemize}
  \item Tuning often starts not with code, but with data structures.
  \item You cannot judge performance without a model.
  \item You cannot optimize what you cannot measure.
  \item Eventually, a better algorithm will always beat a tuning trick.
  \item Your time is worth more than the computer's time.
  \item The fastest code to write may be someone else's library.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
  Enough with the warm-up act. \\
  Floating point!
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Why this lecture?}

  Isn't this really a lecture for the start of CS 3220?
  \begin{itemize}
  \item Except you might have forgotten some things
  \item And might care about using single precision for speed
  \item And might wonder when your FP code starts to crawl
  \item And may want to run code on a current GPU
  \item And may care about mysterious hangs in parallel code
  \item And may wonder about reproducible results in parallel
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Some history}
  
  \begin{itemize}
  \item Von Neumann and Goldstine, 1947 -- can't solve linear systems
    accurately for $n > 15$ without carrying many digits ($n > 8$). \\
  \item Turing, 1949 -- carrying $d$ digits is equivalent to changing input
    data in the $d$th place (backward error analysis)
  \item Wilkinson, 1961 -- rediscovered + publicized backward error analysis 
    (1970 Turing Award)
  \item Backward error analysis of standard algorithms from 1960s on
  \item But varying arithmetics made portable numerical SW hard!
  \item IEEE 754/854 floating point standards \\
    (published 1985; Turing award for W. Kahan in 1989)
  \item Revised IEEE 754 standard in 2008
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{IEEE floating point reminder}

  Normalized numbers:
  \[
    (-1)^s \times (1.b_1 b_2 \ldots b_p)_2 \times 2^e
  \]
  Have 32-bit single, 64-bit double numbers consisting of
  \begin{itemize}
  \item Sign $s$
  \item Precision $p$ ($p = 23$ or $52$)
  \item Exponent $e$ ($-126 \leq e \leq 126$ or $-1022 \leq e \leq 1023$)
  \end{itemize}

  \vspace{5mm}
  Questions:
  \begin{itemize}
  \item What if we can't represent an exact result?
  \item What about $2^{e_{\max}+1} \leq x < \infty$ or $0 \leq x < 2^{e_{\min}}$?
  \item What if we compute $1/0$?
  \item What if we compute $\sqrt{-1}$?
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Rounding}

  Basic ops ($+, -, \times, /, \sqrt{}$), require {\em correct rounding}
  \begin{itemize}
  \item As if computed to infinite precision, then rounded.
    \begin{itemize}
    \item Don't actually need infinite precision for this!
    \end{itemize}
  \item Different rounding rules possible:
    \begin{itemize}
    \item Round to nearest even (default)
    \item Round up, down, toward 0 -- error bounds and intervals
    \end{itemize}
  \item If rounded result $\neq$ exact result, have {\em inexact exception}
    \begin{itemize}
    \item Which most people seem not to know about...
    \item ... and which most of us who do usually ignore
    \end{itemize}
  \item 754-2008 {\em recommends} (does not require) correct rounding
    for a few transcendentals as well (sine, cosine, etc).
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Denormalization and underflow}

  Denormalized numbers:
  \[
    (-1)^s \times (0.b_1 b_2 \ldots b_p)_2 \times 2^{e_{\min}}
  \]
  \begin{itemize}
  \item Evenly fill in space between $\pm 2^{e_{\min}}$
  \item Gradually lose bits of precision as we approach zero
%  \item Required to guarantee $x-y = 0 \implies x = y$
  \item Denormalization results in an {\em underflow exception}
    \begin{itemize}
    \item Except when an exact zero is generated
    \end{itemize}
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Infinity and NaN}

  Other things can happen:
  \begin{itemize}
  \item $2^{e_{\max}} + 2^{e_{\max}}$ generates $\infty$ ({\em overflow exception})
  \item $1/0$ generates $\infty$ ({\em divide by zero exception})
    \begin{itemize}
    \item ... should really be called ``exact infinity'' exception
    \end{itemize}
  \item $\sqrt{-1}$ generates Not-a-Number ({\em invalid exception})
  \end{itemize}
  But every basic operation produces {\em something} well defined.
\end{frame}


\begin{frame}
  \frametitle{Basic rounding model}

  Model of roundoff in a basic op:
  \[
    \fl(a \odot b) = (a \odot b)(1 + \delta), \quad
    |\delta| \leq \macheps.
  \]
  \begin{itemize}
  \item This model is {\em not} complete
    \begin{itemize}
    \item Too optimistic: misses overflow, underflow, or divide by zero
    \item Also too pessimistic -- some things are done exactly!
    \item Example: $2x$ exact, as is $x+y$ if $x/2 \leq y \leq 2x$
    \end{itemize}
  \item But useful as a basis for backward error analysis
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Horner's rule}

Evaluate $p(x) = \sum_{k=0}^n c_k x^k$:
\begin{verbatim}
p = c(n)
for k = n-1 downto 0
  p = x*p + c(k)
\end{verbatim}

\vspace{5mm}
Can show backward error result:
\[
  \fl(p) = \sum_{k=0}^n \hat{c}_k x^k
\]
where $|\hat{c}_k-c_k| \leq (n+1) \macheps |c_k|$.

\vspace{5mm}
Backward error + sensitivity gives forward error.  Can even
compute running error estimates!

\end{frame}


\begin{frame}[fragile]
  \frametitle{Hooray for the modern era!}

  \begin{itemize}
  \item Almost everyone implements IEEE 754 (at least 1985)
    \begin{itemize}
    \item Old Cray arithmetic is essentially extinct
    \end{itemize}
  \item We teach backward error analysis in basic classes
  \item We have good libraries for linear algebra, 
        elementary functions
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Back to the future?}

  \begin{itemize}
  \item Almost everyone implements IEEE 754 (at least 1985)
    \begin{itemize}
    \item Old Cray arithmetic is essentially extinct
    \item But GPUs may lack gradual underflow
    \item And it's impossible to write portable exception handlers
    \item And even with C99, exception flags may be inaccessible
    \item And some features might be slow 
    \item And the compiler might not do what you expected
    \end{itemize}
  \item We teach backward error analysis in basic classes
    \begin{itemize}
    \item ... which are often no longer required!
    \item And anyhow, backward error analysis isn't everything.
    \end{itemize}
  \item We have good libraries for linear algebra, 
        elementary functions
    \begin{itemize}
    \item But people will still roll their own.
    \end{itemize}
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Arithmetic speed}

  Single precision is faster than double precision
  \begin{itemize}
  \item Actual arithmetic cost may be comparable (on CPU)
  \item But GPUs generally prefer single
  \item And SSE instructions do more per cycle with single
  \item And memory bandwidth is lower
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Mixed-precision arithmetic}

  Idea: use double precision only where needed
  \begin{itemize}
  \item Example: iterative refinement and relatives
  \item Or use double-precision arithmetic between
    single-precision representations (may be a good idea regardless)
  \end{itemize}
\end{frame}


\begin{frame}[verbatim]
  \frametitle{Example: Mixed-precision iterative refinement}

\begin{tabbing}
\qquad \= \hspace{4cm} \= \kill
Factor $A = LU$ \>\> $O(n^3)$ single-precision work\\
Solve $x = U^{-1} (L^{-1} b)$ \>\> $O(n^2)$ single-precision work \\
$r = b-Ax$ \>\> $O(n^2)$ double-precision work \\
While $\|r\|$ too large \\
\> $d = U^{-1} (L^{-1} r)$ \> $O(n^2)$ single-precision work\\
\> $x = x+d$ \> $O(n)$ single-precision work \\
\> $r = b-Ax$ \> $O(n^2)$ double-precision work
\end{tabbing}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Helpful extra precision}

\begin{verbatim}
/*
 * Assuming all coordinates are in [1,2), check on which
 * side of the line through A and B is the point C.
 */
int check_side(float ax, float ay, float bx, float by, 
               float cx, float cy)
{
    double abx = bx-ax, aby = by-ay;
    double acx = cx-ax, acy = cy-ay;
    double det = acx*aby-abx*aby;
    if (det == 0) return  0;
    if (det <  0) return -1;
    if (det >  0) return  1;
}
\end{verbatim}
This is not robust if the inputs are double precision!
\end{frame}


\begin{frame}
  \frametitle{Single or double?}

  What to use for:
  \begin{itemize}
  \item Large data sets?  (single for performance, if possible)
  \item Local calculations?  (double by default, except maybe on GPU)
  \item Physically measured inputs?  (probably single)
  \item Nodal coordinates?  (probably single)
  \item Stiffness matrices?  (maybe single, maybe double)
  \item Residual computations?  (probably double)
  \item Checking geometric predicates?  (double or more)
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Simulating extra precision}
  
  What if we want higher precision than is fast?
  \begin{itemize}
  \item Double precision on a GPU?
  \item Quad precision on a CPU?
  \end{itemize}
  Can simulate extra precision.  Example:
\begin{verbatim}
  if abs(a) < abs(b), swap a and b
  double s1 = a+b;         /* May suffer roundoff */
  double s2 = (a-s1) + b;  /* No roundoff! */
\end{verbatim}
  Idea applies more broadly (Bailey, Bohlender, Dekker, Demmel, Hida,
  Kahan, Li, Linnainmaa, Priest, Shewchuk, ...)
  \begin{itemize}
  \item Used in fast extra-precision packages
  \item And in robust geometric predicate code
  \item And in XBLAS
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Exceptional arithmetic speed}

  Time to sum 1000 doubles on my laptop:
  \begin{itemize}
  \item Initialized to 1: 1.3 microseconds
  \item Initialized to inf/nan: 1.3 microseconds
  \item Initialized to $10^{-312}$: 67 microseconds
  \end{itemize}
  $50 \times$ performance penalty for gradual underflow!

  \vspace{5mm}
  Why worry?  Some GPUs don't support gradual underflow
  at all! \\
One reason:
\begin{verbatim}
if (x != y)
    z = x/(x-y);
\end{verbatim}

Also limits range of simulated extra precision.

\end{frame}


\begin{frame}
  \frametitle{Exceptional algorithms, take 2}

  A general idea (works outside numerics, too):
  \begin{itemize}
  \item Try something fast but risky
  \item If something breaks, retry more carefully
  \end{itemize}
  If risky usually works and doesn't cost too much extra,
  this improves performance.

  \vspace{1cm}
  (See Demmel and Li, and also Hull, Farfrieve, and Tang.)

\end{frame}


\begin{frame}
  \frametitle{Parallel problems}
  
  What goes wrong with floating point in parallel (or just high performance)
  environments?
\end{frame}


\begin{frame}
  \frametitle{Problem 1: Repeatability}

  Floating point addition is {\em not} associative:
  \[
    \fl(a + \fl(b + c)) \neq \fl(\fl(a + \b) + c)
  \]

  So answers depends on the inputs, but also
  \begin{itemize}
  \item How blocking is done in multiply or other kernels
  \item Maybe compiler optimizations
  \item Order in which reductions are computed
  \item Order in which critical sections are reached
  \end{itemize}

  \vspace{4mm}
  Worst case: with nontrivial probability we get an answer
  too bad to be useful, not bad enough for the program to barf ---
  and garbage comes out.

\end{frame}


\begin{frame}
  \frametitle{Problem 1: Repeatability}

  What can we do?
  \begin{itemize}
  \item Apply error analysis agnostic to ordering
  \item Write a slower version with specific ordering for debugging
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Problem 2: Heterogeneity}

  \begin{itemize}
  \item Local arithmetic faster than communication
  \item So be redundant about some computation
  \item What if the redundant computations are on different HW?
    \begin{itemize}
    \item Different nodes in the cloud?
    \item GPU and CPU?
    \end{itemize}
  \item Problem case: different exception handling on different nodes
  \item Problem case: take different branches due to different rounding
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Recap}

  So why care about the vagaries of floating point?
  \begin{itemize}
  \item Might actually care about error analysis
  \item Or using single precision for speed
  \item Or maybe just reproducibility
  \item Or avoiding crashes from inconsistent decisions!
  \end{itemize}

  \vspace{5mm}
  Start with ``What Every Computer Scientist Should Know About Floating Point
  Arithmetic'' (David Goldberg, with an addendum by Doug Priest).  It's in
  the back of Patterson-Hennessey.

\end{frame}

\end{document}

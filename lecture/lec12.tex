\documentclass{beamer}

\mode<presentation>
{
%  \usetheme[hideothersubsections]{PaloAlto}
  \usetheme{default}
  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}

\usepackage{amsmath,amssymb}
\usepackage{times} 
\usepackage[T1]{fontenc}

%\newrgbcolor{blue}{0 0 1}
%\newrgbcolor{red}{1 0 0}

\newcommand{\bbR}{\mathbb{R}}
\newcommand{\bbC}{\mathbb{C}}
\newcommand{\bfC}{\mathbf{C}}
\newcommand{\sfC}{\mathsf{C}}
\newcommand{\sfI}{\mathsf{I}}
\newcommand{\bfsigma}{\mathbf{\sigma}}
\newcommand{\rank}{\mathrm{rank}}
\newcommand{\orth}{\mathrm{orth}}
\newcommand{\supp}{\mathrm{supp}}
\newcommand{\tr}{\mathrm{tr}}
\newcommand{\diag}{\mathrm{diag}}
\newcommand{\calF}{\mathcal{F}}
\newcommand{\calG}{\mathcal{G}}
\newcommand{\lambdamin}{\lambda_{\mathrm{min}}}
\newcommand{\lambdamax}{\lambda_{\mathrm{max}}}
\newcommand{\sigmamin}{\sigma_{\mathrm{min}}}
\newcommand{\sigmamax}{\sigma_{\mathrm{max}}}

\newcommand{\myhref}[1]
%{\href{http://www.cs.berkeley.edu/~dbindel/present/movies/#1}}
{\href{run:/Users/dbindel/work/present/movies/#1}}

\title[CS 5220, Spring 2014]{Lecture 12: \\
  Shared memory programming}

\author[]{David Bindel} \date[]{6 Mar 2014}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Logistics}

  \begin{itemize}
  \item HW 1 is graded
  \item Many of you are hacking on HW 2
  \item We're still hacking on HW 3
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Monte Carlo}
  
  Basic idea:  Express answer $a$ as
  \[
    a = E[f(X)]
  \]
  for some random variable(s) $X$.

  \vspace{1cm}
  Typical toy example:
  \[
    \pi/4 = E[\chi_{[0,1]}(X^2 + Y^2)] \mbox{ where } X, Y \sim U(-1,1).
  \]
  We'll be slightly more interesting...

\end{frame}


\begin{frame}
  \frametitle{A toy problem}

  Given ten points $(X_i, Y_i)$ drawn uniformly in $[0,1]^2$,
  what is the expected minimum distance between any pair?
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 1}

Serial version:
\begin{verbatim}
sum_fX = 0;
for i = 1:ntrials
  x = rand(10,2);
  fX = min distance between points in x;
  sum_fX = sum_fX + fx;
end
result = sum_fX/ntrials;
\end{verbatim}
Parallel version: run twice and average results?! \\
No communication --- {\em embarrassingly parallel}

\vspace{5mm}
Need to worry a bit about {\tt rand}...

\end{frame}


\begin{frame}
  \frametitle{Error estimators}

  Central limit theorem: if $R$ is computed result, then
  \[
    R \sim N\left( E[f(X)], \frac{\sigma_{f(X)}}{\sqrt{n}} \right).
  \]
  So:
  \begin{itemize}
  \item
    Compute sample standard deviation $\hat{\sigma_{f(X)}}$
  \item
    Error bars are $\pm \hat{\sigma_{f(X)}}/\sqrt{n}$
  \item
    Use error bars to monitor convergence
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2}

Serial version:
\begin{verbatim}
sum_fX = 0;
sum_fX2 = 0;
for i = 1:ntrials
  x  = rand(10,2);
  fX = min distance between points in x;
  sum_fX  = sum_fX + fX;
  sum_fX2 = sum_fX + fX*fX;
  result = sum_fX/i;
  errbar = sqrt(sum_fX2-sum_fX*sum_fX/i)/i;
  if (abs(errbar/result) < reltol), break; end
end
result = sum_fX/ntrials;
\end{verbatim}
Parallel version: ?

\end{frame}


\begin{frame}
  \frametitle{Pondering parallelism}

  Two major points:
  \begin{itemize}
  \item How should we handle random number generation?
  \item How should we manage termination criteria?
  \end{itemize}

  Some additional points (briefly):
  \begin{itemize}
  \item How quickly can we compute {\tt fX}?
  \item Can we accelerate convergence (variance reduction)?
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Pseudo-random number generation}

  \begin{itemize}
  \item Pretend {\em deterministic} and process is random.
    \begin{itemize}
    \item We lose if it doesn't {\em look} random!
    \item ... but {\em quasi-random} numbers (low-discrepancy
      sequences) are also useful.
    \end{itemize}
  \item RNG functions have {\em state} \\
    $\implies$ Basic {\tt random()} call is {\em not} thread-safe!
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Parallel PRNG strategies}

  \begin{itemize}
  \item Put RNG in critical section (slow)
  \item Run independent RNGs per thread
    \begin{itemize}
    \item Concern: correlation between streams
    \item Requires RNG with a very long period
    \end{itemize}
  \item Split stream from one RNG
    \begin{itemize}
    \item Leapfrog: thread 0 uses even steps, thread 1 uses odd
    \item Block: like leapfrog, but blocked fashion
    \item Helpful if it's cheap to skip steps!  (often the case)
    \end{itemize}
  \end{itemize}
  Good libraries help!  Mersenne twister, SPRNG, ...?

\end{frame}


\begin{frame}[fragile]
  \frametitle{One solution}

  \begin{itemize}
  \item Use a version of Mersenne twister with no global state:
\begin{verbatim}
void sgenrand(long seed, 
              struct mt19937p* mt);
double genrand(struct mt19937p* mt);
\end{verbatim}
  \item Choose pseudo-random seeds per thread at startup:
\begin{verbatim}
long seeds[NTHREADS];
srandom(clock());
for (i = 0; i < NTHREADS; ++i)
    seeds[i] = random();
...
/* sgenrand(seeds[i], mt) for thread i */
\end{verbatim}
  \end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.1p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0;
for each thread in parallel
  do
    fX = result of one random trial
    ++n;
    sum_fX += fX;
    sum_fX2 += fX*fX;
    errbar = ...
    if (abs(errbar/result) < reltol), break; end
  loop
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.2p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0; done = false;
for each thread in parallel
  do
    fX = result of one random trial
    get lock
      ++n;
      sum_fX = sum_fX + fX;
      sum_fX2 = sum_fX2 + fX*fX;
      errbar = ...
      if (abs(errbar/result) < reltol)
        done = true; 
      end
    release lock
  until done
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: Version 2.3p }

\begin{verbatim}
sum_fX = 0; sum_fX2 = 0; n = 0; done = false;
for each thread in parallel
  do
    batch_sum_fX, batch_sum_fX2 = B trials
    get lock
      n += B;
      sum_fX += batch_sum_fX;
      sum_fX2 += batch_sum_fX2;
      errbar = ...
      if (abs(errbar/result) < reltol)
        done = true; 
      end
    release lock
  until done or n > n_max
end
result = sum_fX/n;
\end{verbatim}
\end{frame}


\begin{frame}
  \frametitle{Toy problem: actual code (pthreads)}
  
\end{frame}


\begin{frame}
  \frametitle{Some loose ends}

  \begin{itemize}
  \item Alternative: ``master-slave'' organization
    \begin{itemize}
    \item Master sends out batches of work to slaves
    \item Example: SETI at Home, Folding at Home, ...
    \end{itemize}
  \item What is the right batch size?
    \begin{itemize}
    \item Large $B$ $\implies$ amortize locking/communication overhead \\
          (and variance actually helps with contention!)
    \item Small $B$ avoids too much extra work
    \end{itemize}
  \item How to evaluate $f(X)$?
    \begin{itemize}
    \item For $p$ points, obvious algorithm is $O(p^2)$
    \item Binning points better?  No gain for $p$ small...
    \end{itemize}
  \item Is $f(X)$ the right thing to evaluate?
    \begin{itemize}
    \item Maybe $E[g(X)] = E[f(X)]$ but 
          $\mathrm{Var}[g(X)] \ll \mathrm{Var}[f(X)]$?
    \item May make much more difference than parallelism!
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{The problem with pthreads revisited}
  
  pthreads can be painful!
  \begin{itemize}
  \item Makes code verbose
  \item Synchronization is hard to think about
  \end{itemize}
  
  \vspace{5mm}
  Would like to make this more automatic!
  \begin{itemize}
  \item ... and have been trying for a couple decades.
  \item OpenMP gets us {\em part} of the way
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{OpenMP: Open spec for MultiProcessing}

  \begin{itemize}
  \item Standard API for multi-threaded code
    \begin{itemize}
    \item Only a spec --- multiple implementations
    \item Lightweight syntax
    \item C or Fortran (with appropriate compiler support)
    \end{itemize}
  \item High level:
    \begin{itemize}
    \item Preprocessor/compiler directives (80\%)
    \item Library calls (19\%)
    \item Environment variables (1\%)
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel ``hello world''}

\begin{verbatim}
#include <stdio.h>
#include <omp.h>

int main()
{
    #pragma omp parallel
    printf("Hello world from %d\n", 
           omp_get_thread_num());

    return 0;
}
\end{verbatim}
\end{frame}


\begin{frame}
  \frametitle{Parallel sections}

\begin{center}
\includegraphics[width=0.8\textwidth]{ompsec.pdf}
\end{center}
\begin{itemize}
\item Basic model: fork-join 
\item Each thread runs same code block
\item Annotations distinguish shared ($s$) and private ($i$) data
\item {\em Relaxed consistency} for shared data
\end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel sections}

\begin{center}
\includegraphics[width=0.8\textwidth]{ompsec.pdf}
\end{center}
\begin{verbatim}
...
double s[MAX_THREADS];
int i;
#pragma omp parallel shared(s) private(i)
{
  i = omp_get_thread_num();
  s[i] = i;
}
...
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Critical sections}

  \begin{center}
    \includegraphics[width=\textwidth]{mutex.pdf}
  \end{center}

  \begin{itemize}
  \item Automatically lock/unlock at ends of {\em critical section}
  \item Automatically memory flushes for consistency
  \item Locks are still there if you really need them...
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Critical sections}

  \begin{center}
    \includegraphics[width=\textwidth]{mutex.pdf}
  \end{center}

\begin{verbatim}
#pragma omp parallel {
  ...
  #pragma omp critical my_data_cs
  {
    ... modify data structure here ...
  }
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
\frametitle{Barriers}

  \begin{center}
    \includegraphics[width=\textwidth]{barrier.pdf}
  \end{center}

\begin{verbatim}
#pragma omp parallel
for (i = 0; i < nsteps; ++i) {
  do_stuff
  #pragma omp barrier
}
\end{verbatim}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: actual code (OpenMP)}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Toy problem: actual code (OpenMP)}

  A practical aside...
  \begin{itemize}
  \item OpenMP is supported by Intel and GCC
  \item {\em Not} in main Clang release
  \item I use GCC from Macports for OpenMP on OS X
  \item Need {\tt -fopenmp} for both compile and link lines
\begin{verbatim}
gcc -c -fopenmp foo.c
gcc -o -fopenmp mycode.x foo.o
\end{verbatim}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Parallel loops}

\begin{center}
\includegraphics[width=\textwidth]{omploop.pdf}
\end{center}

\begin{itemize}
\item
  Independent loop body? At least order doesn't matter%
\footnote{If order matters, there's an {\tt ordered} modifier.}.
\item
  Partition index space among threads
\item 
  Implicit barrier at end (except with {\tt nowait})
\end{itemize}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel loops}

\begin{verbatim}
/* Compute dot of x and y of length n */
int i, tid;
double my_dot, dot = 0;
#pragma omp parallel \
        shared(dot,x,y,n) \
        private(i,my_dot)
{
  tid = omp_get_thread_num();
  my_dot = 0;
 
  #pragma omp for
  for (i = 0; i < n; ++i)
    my_dot += x[i]*y[i];

  #pragma omp critical
  dot += my_dot;
}
\end{verbatim}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Parallel loops}

\begin{verbatim}
/* Compute dot of x and y of length n */
int i, tid;
double dot = 0;
#pragma omp parallel \
        shared(x,y,n) \
        private(i) \
        reduction(+:dot)
{
  #pragma omp for
  for (i = 0; i < n; ++i)
    dot += x[i]*y[i];
}
\end{verbatim}

\end{frame}


\begin{frame}
  \frametitle{Parallel loop scheduling}
  
  Partition index space different ways:
  \begin{itemize}
  \item {\tt static[(chunk)]}: decide at start of loop;
    default chunk is {\tt n/nthreads}.  Lowest overhead,
    most potential load imbalance.
  \item {\tt dynamic[(chunk)]}: each thread takes {\tt chunk}
    iterations when it has time; default {\tt chunk} is 1.
    Higher overhead, but automatically balances load.
  \item {\tt guided}: take chunks of size unassigned iterations/threads;
    chunks get smaller toward end of loop.  Somewhere between
    {\tt static} and {\tt dynamic}.
  \item {\tt auto}: up to the system!
  \end{itemize}
  Default behavior is implementation-dependent.

\end{frame}


\begin{frame}
  \frametitle{Other parallel work divisions}
  
  \begin{itemize}
  \item {\tt single}: do only in one thread (e.g. I/O)
  \item {\tt master}: do only in one thread; others skip
  \item {\tt sections}: like cobegin/coend
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Essential complexity?}
  
  Fred Brooks ({\em Mythical Man Month}) identified two types
  of software complexity: essential and accidental.

  \vspace{5mm}
  Does OpenMP address accidental complexity?  Yes, somewhat!

  \vspace{5mm}
  Essential complexity is harder.
\end{frame}


\begin{frame}
  \frametitle{Things to still think about with OpenMP}
  
  \begin{itemize}
  \item Proper serial performance tuning?
  \item Minimizing false sharing?
  \item Minimizing synchronization overhead?
  \item Minimizing loop scheduling overhead?
  \item Load balancing?
  \item Finding enough parallelism in the first place?
  \end{itemize}

  \vspace{1cm}
  Let's focus again on memory issues...
\end{frame}


\begin{frame}
  \frametitle{Memory model}

  \begin{itemize}
  \item Single processor: return last write
    \begin{itemize}
    \item What about DMA and memory-mapped I/O?
    \end{itemize}
  \item Simplest generalization: {\em sequential consistency} -- as if
    \begin{itemize}
    \item Each process runs in program order
    \item Instructions from different processes are interleaved
    \item Interleaved instructions ran on one processor
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Sequential consistency}

  \begin{quote}
    A multiprocessor is sequentially consistent if the result
    of any execution is the same as if the operations of all the
    processors were executed in some sequential order, and the
    operations of each individual processor appear in this sequence
    in the order specified by its program. \\
    \hfill -- Lamport, 1979
  \end{quote}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\end{frame}


\begin{frame}[fragile]
  \frametitle{Example: Spin lock}

Initially, {\tt flag = 0} and {\tt sum = 0}

\vspace{5mm}
\hspace{0.04\textwidth}
\begin{minipage}{0.45\textwidth}
Processor 1:
\begin{verbatim}
sum += p1;
flag = 1;
\end{verbatim}
\end{minipage}
\begin{minipage}{0.45\textwidth}
Processor 2:
\begin{verbatim}
while (!flag);
sum += p2;
\end{verbatim}
\end{minipage}

\vspace{5mm}
Without sequential consistency support, what if
\begin{enumerate}
\item Processor 2 caches {\tt flag}?
\item Compiler optimizes away loop?
\item Compiler reorders assignments on P1?
\end{enumerate}
Starts to look restrictive!

\end{frame}


\begin{frame}
  \frametitle{Sequential consistency: the good, the bad, the ugly}
  
  Program behavior is ``intuitive'':
  \begin{itemize}
  \item Nobody sees garbage values
  \item Time always moves forward
  \end{itemize}
  One issue is {\em cache coherence}:
  \begin{itemize}
  \item Coherence: different copies, same value
  \item Requires (nontrivial) hardware support
  \end{itemize}
  Also an issue for optimizing compiler!

  \vspace{1cm}
  There are cheaper {\em relaxed} consistency models.
\end{frame}


\begin{frame}
  \frametitle{Snoopy bus protocol}

  % Need picture
  Basic idea:
  \begin{itemize}
  \item Broadcast operations on memory bus
  \item Cache controllers ``snoop'' on all bus transactions
    \begin{itemize}
    \item Memory writes induce serial order
    \item Act to enforce coherence (invalidate, update, etc)
    \end{itemize}
  \end{itemize}

  Problems:
  \begin{itemize}
  \item Bus bandwidth limits scaling
  \item Contending writes are slow
  \end{itemize}

  \vspace{5mm}
  There are other protocol options (e.g. directory-based). \\
  But usually give up on {\em full} sequential consistency.

\end{frame}


\begin{frame}
  \frametitle{Weakening sequential consistency}

  Try to reduce to the {\em true} cost of sharing
  \begin{itemize}
  \item {\tt volatile} tells compiler when to worry about sharing
  \item Memory fences tell when to force consistency
  \item Synchronization primitives (lock/unlock) include fences
  \end{itemize}
  
\end{frame}


\begin{frame}
  \frametitle{Sharing}
  
  True sharing:
  \begin{itemize}
  \item Frequent writes cause a bottleneck.
  \item Idea: make independent copies (if possible).
  \item Example problem: malloc/free data structure.
  \end{itemize}
  
  \vspace{5mm}
  False sharing:
  \begin{itemize}
  \item Distinct variables on same cache block
  \item Idea: make processor memory contiguous (if possible)
  \item Example problem: array of ints, one per processor
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Take-home message}

  \begin{itemize}
  \item Sequentially consistent shared memory is a useful idea...
    \begin{itemize}
    \item ``Natural'' analogue to serial case
    \item Architects work hard to support it
    \end{itemize}
  \item ... but implementation is costly!
    \begin{itemize}
    \item Makes life hard for optimizing compilers
    \item Coherence traffic slows things down
    \item Helps to limit sharing
    \end{itemize}
  \end{itemize}
  \vspace{1cm}
  Have to think about these things to get good performance.

\end{frame}


\end{document}
